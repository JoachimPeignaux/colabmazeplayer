using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PointerManager : MonoBehaviour
{
    //public List<IClickable> clickables;
    IClickable currentlySelected;
    Camera cam;

    float pressedDuration;
    bool hasCalledHandleDown;
    [SerializeField] readonly float clickDurationLimit = 0.3f;

    void Awake()
    {
       // clickables = new List<IClickable>();
        cam = Camera.main;
        hasCalledHandleDown = false;
        pressedDuration = 0;
    }

   /* void FindIClickable()
    {
        clickables.Clear();
        foreach ( IClickable clickable in FindObjectsOfType(typeof(MonoBehaviour)).OfType<IClickable>()) {
            clickables.Add(clickable);
        }
    }*/

    private void Start()
    {
        //FindIClickable();
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pressedDuration = 0;
            hasCalledHandleDown = false;
            currentlySelected = FindSelectedItem();
            currentlySelected?.OnFirstClick();
        }

        if (Input.GetMouseButton(0)) {
            pressedDuration += Time.deltaTime;
            if (pressedDuration >= clickDurationLimit) {
                if (!hasCalledHandleDown) {
                    currentlySelected?.HandleDown();
                    hasCalledHandleDown = true;
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (pressedDuration >= clickDurationLimit) {
                currentlySelected?.HandleUp();
            } else {
                currentlySelected?.HandleClick();
            }
            currentlySelected = null;
        }
        
        currentlySelected?.HandleUpdate();
    }

    private IClickable FindSelectedItem() {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit)) //Layer ground
        {
            return hit.transform.GetComponent<IClickable>();
        }
        return null;
    }

}
