using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float sensitivity = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        float tx = Input.GetAxis("Mouse X") * sensitivity;
        float ty = Input.GetAxis("Mouse Y") * sensitivity;
        Quaternion currentRotationInverse = Quaternion.Inverse(transform.localRotation);
        Quaternion additionalRotation = Quaternion.AngleAxis(tx, currentRotationInverse * Vector3.up) * Quaternion.AngleAxis(ty, currentRotationInverse * Vector3.right);
        transform.localRotation *= additionalRotation;
    }
}
