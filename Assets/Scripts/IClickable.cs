using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClickable
{
    public void HandleDown();
    public void HandleUpdate();
    public void HandleUp();
    public void HandleClick();

    public void OnFirstClick();
}
