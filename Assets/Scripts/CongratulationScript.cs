using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CongratulationScript : MonoBehaviour
{
    CanvasGroup group;
    [SerializeField] float fadeDuration;
    float t;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
        group.alpha = 0;
    }

    public void PrintMessage() {
        t = 0;
        StartCoroutine(Fade());
    }

    IEnumerator Fade() {
        while (t <= fadeDuration)
        {
            t += Time.deltaTime;
            group.alpha = Mathf.Clamp(1 - (t / fadeDuration),0,1);
            yield return null;
        }
    }

}
