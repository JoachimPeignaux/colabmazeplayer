using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;

public class Snapable : MonoBehaviour, IClickable
{
    static List<TileHolder> snapPoints;
    static Camera cam;
    readonly float snapDistance = 2;

    private Vector3 initialScale;
    private Vector3 zoomScale;
    private Vector3 currentPosition;
    private Vector3 previousPosition;
    private Quaternion initialRotation;
    private Transform previousParent;
    private Tween currentTween;

    public int tileNumber;

    public void HandleClick()
    {
        if (!enabled)
            return;

        initialRotation = transform.rotation;
        ZoomSpot.Instance.gameObject.SetActive(true);
        previousParent = transform.parent;
        currentTween.Kill();
        currentTween = DOTween.Sequence()
            .Append(transform.DOMove(ZoomSpot.Instance.transform.position, 0.3f))
            .Join(transform.DOScale(zoomScale, 0.3f));
        transform.parent = ZoomSpot.Instance.transform;
    }

    public void OnFirstClick() {
        previousPosition = transform.position;
    }

    public void HandleDown()
    {
        
    }

    public void HandleUp()
    {
        Maze.Instance.CheckChildren();
    }

    private void Awake()
    {
        if (snapPoints == null) {
            snapPoints = new List<TileHolder>();
            foreach (TileHolder tile in FindObjectsOfType(typeof(MonoBehaviour)).OfType<TileHolder>()) {
                snapPoints.Add(tile);
            }
        }
        if (cam == null)
            cam = Camera.main;
        
        currentPosition = transform.position;
        initialScale = transform.localScale;
        zoomScale = initialScale * 2.0f;
    }

    public void HandleUpdate() {

        if (!enabled)
            return;

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 500, 64)) //Layer ground
        {
            transform.position = hit.point + Vector3.up * 1.14f;
            currentPosition = transform.position;
        }

        float smallestDist = float.MaxValue;
        TileHolder closestTile = snapPoints[0];
        foreach (TileHolder tile in snapPoints) {
            if (tile.IsFree || tile.transform == transform.parent)
            {
                float distance = (transform.position - tile.transform.position).sqrMagnitude;
                if (distance < smallestDist)
                {
                    smallestDist = distance;
                    closestTile = tile;
                }
            }
        }

        if (transform.parent != closestTile)
        {
            if (transform.parent)
            {
                TileHolder holder = transform.parent.GetComponent<TileHolder>();
                if (holder)
                {
                    holder.IsFree = true;
                }
            }
            transform.parent = null;
        }

        if (smallestDist <= snapDistance * snapDistance)
        {
            transform.position = closestTile.transform.position + Vector3.up * 0.15f;
            transform.parent = closestTile.transform;
            closestTile.IsFree = false;
        }

    }

    public void ReturnAtUnzoomPosition() {
        currentTween.Kill();
        currentTween = DOTween.Sequence()
            .Append(transform.DOMove(previousPosition, 0.3f))
            .Join(transform.DOScale(initialScale, 0.3f))
            .Join(transform.DORotateQuaternion(initialRotation, 0.3f).SetRelative(false));
        transform.parent = previousParent;
    }

    private void OnDestroy()
    {
        if (snapPoints != null)
        {
            snapPoints.Clear();
            snapPoints = null;
        }
        cam = null;
    }

}
