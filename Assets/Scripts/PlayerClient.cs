using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerClient : MonoBehaviour
{
    [SerializeField] private string m_hostIP;
    [SerializeField] private int m_port;
    [SerializeField] private string m_connectionMessage;

    [SerializeField] private UnityEvent m_buzzerSolved;
    private bool hasReceivedBuzzer;

    [SerializeField] private UnityEvent m_chestOpened;
    private bool hasOpenedChest;

    TcpClient client;
    NetworkStream stream;

    const char msgSeparator = '/';

    public static PlayerClient Instance;

    public void Awake()
    {
        Instance = this;
    }


    private void Start()
    {
        hasReceivedBuzzer = false;
        hasOpenedChest = false;
        Connect(m_hostIP, m_port, m_connectionMessage);
        StartCoroutine(ReadMessages());
    }

    void Connect(String server, int port, string message)
    {
        try
        {
            // Create a TcpClient.
            // Note, for this client to work you need to have a TcpServer
            // connected to the same address as specified by the server, port
            // combination.
            client = new TcpClient(server, port);


            // Get a client stream for reading and writing.
            //  Stream stream = client.GetStream();
            stream = client.GetStream();

            SendMessageToClient(message);

            // String to store the response ASCII representation.
            String responseData = String.Empty;

            // Read the first batch of the TcpServer response bytes.
            string msg;
            while (true)
            {
                if (TryRead(out msg)) {
                    Debug.Log("client message received: " + msg);
                    break;
                }
            }

        }
        catch (ArgumentNullException e)
        {
            Debug.LogError("ArgumentNullException: {0}" + e);
        }
        catch (SocketException e)
        {
            Debug.LogError("SocketException: {0}" + e);
        }
    }

    public void SendMessageToClient(string message) {

        // Translate the passed message into ASCII and store it as a Byte array.
        Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

        // Send the message to the connected TcpServer.
        stream.Write(data, 0, data.Length);

        Debug.Log("Sent: {0}" + message);
    }

    private bool TryRead(out string msg)
    {
        if (stream.DataAvailable)
        {
            Byte[] bytes = new Byte[1024];
            int at = 0;

            int length = 0;
            int data;
            while (stream.DataAvailable && (data = stream.ReadByte()) != msgSeparator)
            {
                bytes[at++] = (Byte)data;
                length++;
            }
            var incommingData = new Byte[length];

            Array.Copy(bytes, 0, incommingData, 0, length);

            msg = Encoding.ASCII.GetString(incommingData);
            return true;
        }

        msg = "";
        return false;
    }

    void CloseConnection()
    {
        // Close everything.
        stream.Close();
        client.Close();
    }

    IEnumerator ReadMessages() {
        string msg;
        while (true)
        {
            if (TryRead(out msg))
            {
                Debug.Log("client message received: " + msg);
                if (msg == "BUZZER_ENIGMA_SOLVED" && !hasReceivedBuzzer) {
                    m_buzzerSolved.Invoke();
                    Maze.Instance.CheckChildren();
                    hasReceivedBuzzer = true;
                } else if (msg == "CHEST_UNLOCKED" && !hasOpenedChest) {
                    m_chestOpened.Invoke();
                    hasOpenedChest = true;
                }
                else if (msg == "RESET")
                {
                    SceneManager.LoadScene(0);
                }
            }
            yield return null;
        }
    }

    private void OnApplicationQuit()
    {
        CloseConnection();
    }

}