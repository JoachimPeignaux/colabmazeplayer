using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNewTiles : MonoBehaviour
{
    [SerializeField] List<GameObject> newTiles;

    private void Awake()
    {
        foreach (GameObject tile in newTiles)
        {
            tile.SetActive(false);
        }
    }

    public void ActiveTiles() {
        foreach (GameObject tile in newTiles)
        {
            tile.SetActive(true);
        }
    }

}
