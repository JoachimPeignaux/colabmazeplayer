using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerIntoWorld : MonoBehaviour
{
    Camera cam;

    private void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) {
            transform.position = hit.point;
        }
        
    }
}
