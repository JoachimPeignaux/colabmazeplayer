using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class Maze : MonoBehaviour, IClickable
{
    private Vector3 initialScale;
    private Vector3 zoomScale;
    Vector3 initialPosition;
    Quaternion initialRotation;
    Transform previousParent;

    public static Maze Instance;
    [SerializeField] int correctTilesBeforeChest;
    [SerializeField] GameObject chestGO;
    [SerializeField] UnityEvent printEnd;
    
    private bool hasDisplayedChest;
    private bool hasFinsihed;
    private bool buzzerSolved;
    private Tween currentTween;

    public void Awake() {
        Instance = this;
        hasDisplayedChest = false;
        buzzerSolved = false;
        hasFinsihed = false;
        initialPosition = transform.position;
        initialRotation = transform.rotation;
        initialScale = transform.localScale;
        zoomScale = initialScale * 2.0f;
        if(chestGO!=null)
            chestGO.SetActive(false);
    }

    public void HandleClick()
    {
        ZoomSpot.Instance.gameObject.SetActive(true);
        previousParent = transform.parent;
        currentTween.Kill();
        currentTween = DOTween.Sequence()
            .Append(transform.DOMove(ZoomSpot.Instance.transform.position, 0.3f))
            .Join(transform.DOScale(zoomScale, 0.3f));
        transform.parent = ZoomSpot.Instance.transform;
    }

    public void OnFirstClick() {}

    public void HandleDown()
    {

    }

    public void HandleUp()
    {

    }

    public void HandleUpdate()
    {

    }

    public void ReturnAtUnzoomPosition() {
        currentTween.Kill();
        currentTween = DOTween.Sequence()
            .Append(transform.DOMove(initialPosition, 0.3f))
            .Join(transform.DOScale(initialScale, 0.3f))
            .Join(transform.DOLocalRotateQuaternion(initialRotation, 0.3f));
        transform.parent = previousParent;
    }

    public void CheckChildren() {
        int goodCount = 0;
        foreach (Transform child in transform) {
            if (child.childCount != 2)
            {
                continue;
            }
            int tileHolderNumber = child.GetComponent<TileHolder>().tileNumber;
            if (tileHolderNumber != child.GetChild(1).GetComponent<Snapable>().tileNumber || tileHolderNumber == 0) {
                continue;
            }
            goodCount++;
        }

        Debug.Log(buzzerSolved +  "buzze solved _ " + goodCount + "good count");
        if (goodCount == transform.childCount && buzzerSolved)
        {
            AllTileOrdered();
        }
        else if (goodCount >= correctTilesBeforeChest && !hasDisplayedChest)
        {
            DisplayChest();
        }
    }

    void DisplayChest() {
        if(chestGO!=null)
            chestGO.SetActive(true);
        hasDisplayedChest = true;
        foreach (Transform child in transform)
        {
            if (child.childCount != 2)
            {
                continue;
            }
            int tileHolderNumber = child.GetComponent<TileHolder>().tileNumber;
            if (tileHolderNumber != child.GetChild(1).GetComponent<Snapable>().tileNumber || tileHolderNumber == 0)
            {
                continue;
            }
            child.GetChild(1).GetComponent<Snapable>().enabled = false;
        }
        PlayerClient.Instance.SendMessageToClient("CHEST_DISCOVERED");
    }

    void AllTileOrdered() {
        PlayerClient.Instance.SendMessageToClient("TILE_ORDERED");
        hasFinsihed = true;
        foreach (Snapable snapable in GameObject.FindObjectsOfType(typeof(Snapable)))
        {
            snapable.enabled = false;
        }
        printEnd.Invoke();
    }

    public void BuzzerSolved() {
        buzzerSolved = true;
    }

}
