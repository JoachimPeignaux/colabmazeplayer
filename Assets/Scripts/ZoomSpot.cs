using UnityEngine;

public class ZoomSpot : MonoBehaviour, IClickable
{
    Vector2 turn;
    [SerializeField] float sensitivity = 2f;
    static public ZoomSpot Instance;

    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    public void HandleClick()
    {
        transform.GetChild(0).SendMessage("ReturnAtUnzoomPosition");
        this.gameObject.SetActive(false);
    }

    public void OnFirstClick() {}

    public void HandleDown()
    {
        
    }

    public void HandleUp()
    {
        
    }

    public void HandleUpdate()
    {
        float tx = Input.GetAxis("Mouse X") * sensitivity;
        float ty = Input.GetAxis("Mouse Y") * sensitivity;
        Quaternion currentRotationInverse = Quaternion.Inverse(transform.localRotation);
        Quaternion additionalRotation = Quaternion.AngleAxis(-tx, currentRotationInverse * Vector3.up) * Quaternion.AngleAxis(ty, currentRotationInverse * Vector3.right);
        if (transform.childCount > 0) {
            transform.GetChild(0).localRotation *= additionalRotation;
        }
    }

}
